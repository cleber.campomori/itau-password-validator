package br.com.itau.password.validator.web.config

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Profile
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter
import org.springframework.web.client.RestTemplate

@Configuration
@ComponentScan("br.com.itau")
@Profile("test")
@Import(value = [ObjectMapperConfig::class, SwaggerConfig::class])
class WebTestConfig {

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Bean
    fun restTemplate(): RestTemplate {
        val restTemplate = RestTemplate()
        val jsonMessageConverter = MappingJackson2HttpMessageConverter()
        jsonMessageConverter.objectMapper = objectMapper
        restTemplate.messageConverters[0] = jsonMessageConverter
        return restTemplate
    }
}
