package br.com.itau.password.validator.web.controllers

import br.com.itau.password.validator.web.base.BaseIT
import br.com.itau.password.validator.web.exceptions.InvalidPasswordException
import br.com.itau.password.validator.web.fixtures.PasswordRequestFixtures
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.client.HttpClientErrorException
import org.springframework.web.client.RestTemplate

class PasswordControllerTestsIT : BaseIT() {

    @Autowired
    private lateinit var restTemplate: RestTemplate

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Test
    fun `ensure an invalid password request will not be validated`() {
        val httpException: HttpClientErrorException = assertThrows {
            restTemplate.postForEntity(
                testRoute("${PasswordController.BASE_ROUTE}"),
                PasswordRequestFixtures.passwordRequestViolatingJustOneRule(),
                Any::class.java
            )
        }
        assertTrue(
            httpException.statusCode == HttpStatus.BAD_REQUEST,
            "An unexpected HTTP status was found for a invalid password request [expected = 400, found = ${httpException.statusCode}]"
        )
        val invalidPasswordException: InvalidPasswordException = objectMapper.readValue(
            httpException.responseBodyAsString
        )
        assertTrue(
            invalidPasswordException.errors.size == 1,
            "More than 1 errors were retrieved for a password with just only 1 error [${invalidPasswordException.errors}]"
        )
    }

    @Test
    fun `ensure an invalid password request with multiple errors will not be validated`() {
        val httpException: HttpClientErrorException = assertThrows {
            restTemplate.postForEntity(
                testRoute("${PasswordController.BASE_ROUTE}"),
                PasswordRequestFixtures.passwordRequestViolatingMoreThanOneRule(),
                Any::class.java
            )
        }
        assertTrue(
            httpException.statusCode == HttpStatus.BAD_REQUEST,
            "An unexpected HTTP status was found for a invalid password request [expected = 400, found = ${httpException.statusCode}]"
        )
        val invalidPasswordException: InvalidPasswordException = objectMapper.readValue(
            httpException.responseBodyAsString
        )
        assertTrue(
            invalidPasswordException.errors.size > 1,
            "More than 1 errors were retrieved for a password with just only 1 error [${invalidPasswordException.errors}]"
        )
    }

    @Test
    fun `ensure an valid password request will be validated`() {
        val response: ResponseEntity<Any> = restTemplate.postForEntity(
            testRoute("${PasswordController.BASE_ROUTE}"),
            PasswordRequestFixtures.validPasswordRequest(),
            Any::class.java
        )
        assertTrue(
            response.statusCode == HttpStatus.NO_CONTENT,
            "An unexpected HTTP status was found for a valid password request [expected = 204, found = ${response.statusCode}]"
        )
    }
}
