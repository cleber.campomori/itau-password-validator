package br.com.itau.password.validator.web.fixtures

import br.com.itau.password.validator.dtos.PasswordRequest

object PasswordRequestFixtures {

    fun passwordRequestViolatingJustOneRule(): PasswordRequest = PasswordRequest(
        password = "abc21345@"
    )

    fun passwordRequestViolatingMoreThanOneRule(): PasswordRequest = PasswordRequest(
        password = "abc@"
    )

    fun validPasswordRequest(): PasswordRequest = PasswordRequest(
        password = "AbTp9!fok"
    )
}
