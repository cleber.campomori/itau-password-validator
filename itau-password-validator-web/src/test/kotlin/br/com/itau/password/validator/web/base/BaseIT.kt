package br.com.itau.password.validator.web.base

import br.com.itau.password.validator.web.config.WebTestConfig
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = [WebTestConfig::class]
)
abstract class BaseIT {

    @LocalServerPort
    private val serverPort: Int = 9000

    fun testRoute(target: String, params: Map<String, Any>? = null): String =
        if (params.isNullOrEmpty()) {
            "http://localhost:$serverPort$target"
        } else {
            val queryString: String = params.map { (k: String, v: Any) ->
                "$k=$v"
            }.joinToString(separator = "&")
            "http://localhost:$serverPort$target?$queryString"
        }
}
