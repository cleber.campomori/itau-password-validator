package br.com.itau.password.validator.web.extensions

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.web.fixtures.PasswordRequestFixtures
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PasswordRequestExtensionsTest {

    @Test
    fun `ensure a password request is converted to a password definition`() {
        val passwordDefinition: PasswordDefinition =
            PasswordRequestFixtures.validPasswordRequest().toPasswordDefinition()
        assertTrue(
            passwordDefinition.password == PasswordRequestFixtures.validPasswordRequest().password,
            "The password request was not correctly converted to a password definition: the passwords are different"
        )
    }
}
