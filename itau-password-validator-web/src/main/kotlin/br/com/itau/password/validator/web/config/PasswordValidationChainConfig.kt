package br.com.itau.password.validator.web.config

import br.com.itau.password.validator.services.validation.handlers.chain.PasswordValidationChain
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastNineCharactersValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneDigitCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneLowercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneSpecialCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneUppercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.NotContainRepeatedCharactersValidationHandler
import org.springframework.context.annotation.Bean

class PasswordValidationChainConfig {

    @Bean
    fun passwordValidationChain() =
        PasswordValidationChain()
            .addValidationHandler(AtLeastNineCharactersValidationHandler())
            .addValidationHandler(AtLeastOneUppercaseCharacterValidationHandler())
            .addValidationHandler(AtLeastOneLowercaseCharacterValidationHandler())
            .addValidationHandler(AtLeastOneDigitCharacterValidationHandler())
            .addValidationHandler(AtLeastOneSpecialCharacterValidationHandler())
            .addValidationHandler(NotContainRepeatedCharactersValidationHandler())
}
