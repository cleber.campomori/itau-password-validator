package br.com.itau.password.validator.web.config

import org.springframework.context.annotation.Bean
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@EnableSwagger2
class SwaggerConfig {

    @Bean
    fun api(): Docket =
        Docket(DocumentationType.SWAGGER_2)
            .select()
            .apis(RequestHandlerSelectors.basePackage("br.com.itau"))
            .paths(PathSelectors.any())
            .build()
            .useDefaultResponseMessages(false)
            .apiInfo(
                ApiInfoBuilder()
                    .title("Itaú Password Validator API")
                    .description("API for validate password by applying some rules")
                    .contact(
                        Contact(
                            "Cleber Lopes Campomori",
                            "https://github.com/clebercampomori",
                            "cleber.campomori@gmail.com"
                        )
                    )
                    .build()
            )
}
