package br.com.itau.password.validator.web.extensions

import br.com.itau.password.validator.dtos.PasswordRequest
import br.com.itau.password.validator.models.PasswordDefinition

fun PasswordRequest.toPasswordDefinition(): PasswordDefinition = PasswordDefinition(
    password = this.password
)
