package br.com.itau.password.validator.web.exceptions

class InvalidPasswordException(
    val errors: List<String>
) : Exception("Some errors were detected while validation password [${errors.size} errors]")
