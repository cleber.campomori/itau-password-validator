package br.com.itau.password.validator.web.controllers

import br.com.itau.password.validator.dtos.PasswordRequest
import br.com.itau.password.validator.services.core.PasswordService
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.web.controllers.PasswordController.Companion.BASE_ROUTE
import br.com.itau.password.validator.web.exceptions.InvalidPasswordException
import br.com.itau.password.validator.web.extensions.toPasswordDefinition
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(
    BASE_ROUTE,
    produces = [MediaType.APPLICATION_JSON_VALUE],
    consumes = [MediaType.APPLICATION_JSON_VALUE]
)
@Api(value = "Password Validation Controller", description = "Controller to perform password validation actions")
class PasswordController(
    private val passwordService: PasswordService
) {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        const val BASE_ROUTE: String = "/passwords"
    }

    @PostMapping
    @ApiOperation(
        value = "Validate a password request",
        httpMethod = "POST",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiResponses(
        value = [
            ApiResponse(code = 204, message = "The supplied password is valid"),
            ApiResponse(
                code = 400,
                message = "The supplied password is considered as invalid. The reason is on the response body."
            ),
            ApiResponse(code = 500, message = "Some unexpected error has occurred")
        ]
    )
    fun validatePassword(
        @ApiParam(name = "Request", value = "Request to specify the password to be validated", required = true)
        @RequestBody passwordRequest: PasswordRequest
    ): ResponseEntity<Void> {
        logger.debug("Trying to validate a password request [password omitted]")
        val validationResult: PasswordValidationNotification =
            passwordService.validatePassword(passwordRequest.toPasswordDefinition())
        if (validationResult.isValid()) {
            logger.debug("OK! The password is valid. Replying with success status code")
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build()
        } else {
            throw InvalidPasswordException(errors = validationResult.errors())
        }
    }
}
