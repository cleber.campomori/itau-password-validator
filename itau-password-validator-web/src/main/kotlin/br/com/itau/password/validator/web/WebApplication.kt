package br.com.itau.password.validator.web

import br.com.itau.password.validator.web.config.ObjectMapperConfig
import br.com.itau.password.validator.web.config.PasswordValidationChainConfig
import br.com.itau.password.validator.web.config.SwaggerConfig
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Import

@SpringBootApplication
@ComponentScan("br.com.itau")
@Import(value = [ObjectMapperConfig::class, SwaggerConfig::class, PasswordValidationChainConfig::class])
class WebApplication

fun main(args: Array<String>) {
    runApplication<WebApplication>(*args)
}
