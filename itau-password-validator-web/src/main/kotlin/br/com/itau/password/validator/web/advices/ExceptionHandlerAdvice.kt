package br.com.itau.password.validator.web.advices

import br.com.itau.password.validator.dtos.InvalidPasswordResponse
import br.com.itau.password.validator.web.exceptions.InvalidPasswordException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestControllerAdvice

@RestControllerAdvice
class ExceptionHandlerAdvice {

    private val logger: Logger = LoggerFactory.getLogger(this::class.java)

    @RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @ExceptionHandler(InvalidPasswordException::class)
    fun handleInvalidPasswordException(exception: InvalidPasswordException): ResponseEntity<InvalidPasswordResponse> {
        logger.info("Dealing with InvalidPasswordException instance [$exception]")
        val invalidPasswordResponse = InvalidPasswordResponse(
            errors = exception.errors
        )
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(invalidPasswordResponse)
    }

    @RequestMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    @ExceptionHandler(Exception::class)
    fun handleGeneralException(exception: InvalidPasswordException): ResponseEntity<InvalidPasswordResponse> {
        logger.info("Dealing with general exception instance [$exception]")
        val invalidPasswordResponse = InvalidPasswordResponse(
            errors = listOf(*exception.errors.toTypedArray())
        )
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(invalidPasswordResponse)
    }
}
