package br.com.itau.password.validator.services.validation.handlers.impl

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler

/**
 * Handler to verify if a password contains at least 1 digit
 */
class AtLeastOneDigitCharacterValidationHandler : PasswordValidationHandler() {

    override val errorMessage: String
        get() = "The password must contain at least 1 digit"

    override fun isValid(passwordDef: PasswordDefinition): Boolean =
        passwordDef.password.any { it.isDigit() }
}
