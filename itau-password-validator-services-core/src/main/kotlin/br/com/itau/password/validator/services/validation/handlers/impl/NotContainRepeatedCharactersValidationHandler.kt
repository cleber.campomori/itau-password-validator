package br.com.itau.password.validator.services.validation.handlers.impl

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler

/**
 * Handler to verify if a password contains at least one capital letter
 */
class NotContainRepeatedCharactersValidationHandler : PasswordValidationHandler() {

    override val errorMessage: String
        get() = "The password must not contain repeated characters"

    override fun isValid(passwordDef: PasswordDefinition): Boolean =
        passwordDef.password.groupBy { it }.all { it.value.count() == 1 }
}
