package br.com.itau.password.validator.services.validation.handlers.base

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification

/**
 * Base class to setup password validation handlers (Chain of Responsibility)
 */
abstract class PasswordValidationHandler {

    private var validationNotification: PasswordValidationNotification = PasswordValidationNotification()
    private var _next: PasswordValidationHandler? = null

    internal abstract val errorMessage: String

    /**
     * Set the next validation handler through the password validation chain
     * @param next The next validation handler to be applied
     * @return The next password validation handler from the chain
     */
    fun setNext(next: PasswordValidationHandler): PasswordValidationHandler {
        next.validationNotification = this.validationNotification
        this._next = next
        return next
    }

    /**
     * Handle the validation of the supplied password definition
     * @return A password validation result object
     */
    fun handle(passwordDef: PasswordDefinition): PasswordValidationNotification {
        if (!isValid(passwordDef)) {
            validationNotification.addError(errorMessage)
        }
        return if (isEndOfChain()) validationNotification else _next!!.handle(passwordDef)
    }

    /**
     * Apply the validation rule
     */
    protected abstract fun isValid(passwordDef: PasswordDefinition): Boolean

    private fun isEndOfChain(): Boolean = _next == null

    /**
     * **INTERNAL** Reset the error state of the associated notification
     */
    internal fun reset() {
        validationNotification.reset()
    }
}
