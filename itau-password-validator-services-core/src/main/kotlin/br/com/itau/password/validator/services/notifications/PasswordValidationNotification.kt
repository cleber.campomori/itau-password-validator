package br.com.itau.password.validator.services.notifications

/**
 * Notify if a password is valid. If the password is invalid, register a list of validation errors
 */
class PasswordValidationNotification {

    private val _errorList: MutableList<String> = mutableListOf()

    /**
     * Register a validation error
     * @param message A message about the validation error
     */
    internal fun addError(message: String) {
        _errorList.add(message)
    }

    /**
     * Returns a list of registered errors
     * @return A list of error messages
     */
    fun errors(): List<String> = listOf(*_errorList.toTypedArray())

    /**
     * Return if the password being validated is valid or not
     * @return true if the password is valid; false otherwise
     */
    fun isValid(): Boolean = _errorList.isEmpty()

    /**
     * **INTERNAL** Reset error state of a password validation notification
     */
    internal fun reset() {
        _errorList.clear()
    }

    override fun toString(): String {
        return "[isValid = ${isValid()}, errors = $_errorList]"
    }
}
