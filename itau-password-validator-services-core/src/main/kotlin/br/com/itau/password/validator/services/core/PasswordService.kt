package br.com.itau.password.validator.services.core

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification

/**
 * Interface for *password service implementations*
 */
interface PasswordService {

    /**
     * Validate a password through specified rules
     * @param passwordDef Password definition to validate
     * @return A password validation notification object, containing possible error messages and a flag indicating if the password is either valid or invalid
     */
    fun validatePassword(passwordDef: PasswordDefinition): PasswordValidationNotification
}
