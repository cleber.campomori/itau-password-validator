package br.com.itau.password.validator.services.validation.handlers.chain

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler

/**
 * Class that allow setup a password validation chain
 */
class PasswordValidationChain {

    private val passwordValidationHandlers: MutableList<PasswordValidationHandler> = mutableListOf()
    private lateinit var passwordDef: PasswordDefinition

    /**
     * Add a new password validation handler to the current chain
     * @param passwordValidationHandler The password validation handler to add
     */
    fun addValidationHandler(passwordValidationHandler: PasswordValidationHandler): PasswordValidationChain =
        apply {
            this.passwordValidationHandlers.add(passwordValidationHandler)
        }

    /**
     * Set the password definition to be validated by the current password validation chain
     */
    fun forPasswordDefinition(passwordDef: PasswordDefinition): PasswordValidationChain = apply {
        this.passwordDef = passwordDef
    }

    /**
     * Apply the password validation chain to the specified password definition
     * @see forPasswordDefinition
     */
    fun apply(): PasswordValidationNotification {
        check(passwordValidationHandlers.isNotEmpty())
        check(this::passwordDef.isInitialized)
        passwordValidationHandlers.forEachIndexed { i: Int, passwordValidationHandler: PasswordValidationHandler ->
            passwordValidationHandler.reset()
            if (i + 1 < passwordValidationHandlers.size) {
                passwordValidationHandler.setNext(passwordValidationHandlers[i + 1])
            }
        }
        return passwordValidationHandlers.first().handle(this.passwordDef)
    }
}
