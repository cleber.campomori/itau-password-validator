package br.com.itau.password.validator.services.validation.handlers.impl

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler

/**
 * Handler to verify if a password contains 9 or more characters
 */
class AtLeastNineCharactersValidationHandler : PasswordValidationHandler() {

    override val errorMessage: String
        get() = "The password contains less than 9 characters"

    override fun isValid(passwordDef: PasswordDefinition): Boolean =
        passwordDef.password.length >= 9
}
