package br.com.itau.password.validator.services.validation.handlers.impl

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler

/**
 * Handler to verify if a password contains 9 or more characters
 */
class AtLeastOneSpecialCharacterValidationHandler : PasswordValidationHandler() {

    override val errorMessage: String
        get() = "he password must contain at least 1 special character"

    override fun isValid(passwordDef: PasswordDefinition): Boolean =
        passwordDef.password.contains(Regex("[^0-9a-zA-Z\\s]"))
}
