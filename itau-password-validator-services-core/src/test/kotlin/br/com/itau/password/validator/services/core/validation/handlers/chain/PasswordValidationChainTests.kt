package br.com.itau.password.validator.services.core.validation.handlers.chain

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.PasswordValidationChainFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.chain.PasswordValidationChain
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertThrows

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PasswordValidationChainTests {

    private lateinit var _passwordValidationChain: PasswordValidationChain

    @BeforeAll
    fun setUpBeforeEachTest() {
        _passwordValidationChain = PasswordValidationChain()
        PasswordValidationChainFixture.fullPasswordValidationHandlerChain()
            .forEach { handler: PasswordValidationHandler ->
                _passwordValidationChain.addValidationHandler(handler)
            }
    }

    @Test
    fun `ensure password is invalid when violating just one validation handler`() {
        val validationResult: PasswordValidationNotification = _passwordValidationChain
            .forPasswordDefinition(PasswordValidationChainFixture.passwordViolatingJustOneValidationHandler())
            .apply()
        assertFalse(
            validationResult.isValid(),
            "The supplied password violates one validation handler, but the validation result was true"
        )
        assertTrue(
            validationResult.errors().size == 1,
            "The supplied password violates just one validation handler, but the validation result contains more than 1 error"
        )
    }

    @Test
    fun `ensure password is invalid when violating more than one validation handler`() {
        val validationResult: PasswordValidationNotification = _passwordValidationChain
            .forPasswordDefinition(PasswordValidationChainFixture.passwordViolatingMoreThanOneValidationHandler())
            .apply()
        assertFalse(
            validationResult.isValid(),
            "The supplied password violates some validation handlers, but the validation result was true"
        )
        assertTrue(
            validationResult.errors().size > 1,
            "The supplied password violates just some validation handlers, but the validation result contains just 1 error"
        )
    }

    @Test
    fun `ensure password is valid when not violating any validation handler`() {
        val validationResult: PasswordValidationNotification = _passwordValidationChain
            .forPasswordDefinition(PasswordValidationChainFixture.passwordNotViolatingValidationHandlerChain())
            .apply()
        assertTrue(
            validationResult.isValid(),
            "The supplied password does not violate any validation handlers, but the validation result was false"
        )
        assertTrue(
            validationResult.errors().isEmpty(),
            "The supplied password does not violates any validation handlers, but the validation result contains errors"
        )
    }

    @Test
    fun `ensure a password chain without handlers will throw an IllegalStateException`() {
        assertThrows<IllegalStateException> {
            PasswordValidationChain()
                .forPasswordDefinition(PasswordValidationChainFixture.passwordNotViolatingValidationHandlerChain())
                .apply()
        }
    }

    @Test
    fun `ensure a password chain without password definition will throw an IllegalStateException`() {
        assertThrows<IllegalStateException> {
            PasswordValidationChain()
                .addValidationHandler(PasswordValidationChainFixture.fullPasswordValidationHandlerChain().first())
                .apply()
        }
    }
}
