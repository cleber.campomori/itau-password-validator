package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object AtLeastNineCharactersValidationHandlerFixture {

    fun passwordDefinitionWith9Characters(): PasswordDefinition = PasswordDefinition(
        password = "123456789"
    )

    fun passwordDefinitionWithMoreThan9Characters(): PasswordDefinition = PasswordDefinition(
        password = "123456789ABC"
    )

    fun passwordDefinitionWithLessThan9Characters(): PasswordDefinition = PasswordDefinition(
        password = "123456"
    )
}
