package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object NotContainRepeatedCharactersValidationHandlerFixture {

    fun passwordDefinitionWithOneRepeatedCharacter(): PasswordDefinition = PasswordDefinition(
        password = "aBC123@a"
    )

    fun passwordDefinitionWithMoreThanOneRepeatedCharacter(): PasswordDefinition = PasswordDefinition(
        password = "aBC123@aB@"
    )

    fun passwordDefinitionWithoutAnyRepeatedCharacter(): PasswordDefinition = PasswordDefinition(
        password = "abcdef123@"
    )
}
