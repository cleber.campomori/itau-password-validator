package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object AtLeastOneDigitCharacterValidationHandlerFixture {

    fun passwordDefinitionWithOneDigitCharacter(): PasswordDefinition = PasswordDefinition(
        password = "ABC1DEF"
    )

    fun passwordDefinitionWithMoreThanOneDigitCharacter(): PasswordDefinition = PasswordDefinition(
        password = "ABC123DEF"
    )

    fun passwordDefinitionWithoutAnyDigitCharacter(): PasswordDefinition = PasswordDefinition(
        password = "ABCDEF@@"
    )
}
