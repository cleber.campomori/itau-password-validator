package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object AtLeastOneLowercaseCharacterValidationHandlerFixture {

    fun passwordDefinitionWithOneLowercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "aBC123DEF"
    )

    fun passwordDefinitionWithMoreThanOneLowercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "abC123DEF"
    )

    fun passwordDefinitionWithoutAnyLowercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "ABCDEF@@123"
    )
}
