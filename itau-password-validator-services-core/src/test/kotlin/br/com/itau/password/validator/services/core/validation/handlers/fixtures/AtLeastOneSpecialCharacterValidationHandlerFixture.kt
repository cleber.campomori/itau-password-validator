package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object AtLeastOneSpecialCharacterValidationHandlerFixture {

    fun passwordDefinitionWithOneSpecialCharacter(): PasswordDefinition = PasswordDefinition(
        password = "aBC123DEF@"
    )

    fun passwordDefinitionWithMoreThanOneSpecialCharacter(): PasswordDefinition = PasswordDefinition(
        password = "abC123DEF@!"
    )

    fun passwordDefinitionWithoutAnySpecialCharacter(): PasswordDefinition = PasswordDefinition(
        password = "ABCDeF123"
    )
}
