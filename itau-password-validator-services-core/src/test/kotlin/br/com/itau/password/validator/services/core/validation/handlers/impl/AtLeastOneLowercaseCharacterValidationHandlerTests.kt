package br.com.itau.password.validator.services.core.validation.handlers.impl

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.AtLeastOneLowercaseCharacterValidationHandlerFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneLowercaseCharacterValidationHandler
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AtLeastOneLowercaseCharacterValidationHandlerTests {

    private val _passwordValidationHandler: PasswordValidationHandler = AtLeastOneLowercaseCharacterValidationHandler()

    @Test
    fun `ensure a password with 1 lowercase char is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneLowercaseCharacterValidationHandlerFixture.passwordDefinitionWithOneLowercaseCharacter())
        assertTrue(
            result.isValid(),
            "The password contains 1 lowercase char, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password with 1 lowercase character"
        )
    }

    @Test
    fun `ensure a password with more than 1 lowercase char is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneLowercaseCharacterValidationHandlerFixture.passwordDefinitionWithMoreThanOneLowercaseCharacter())
        assertTrue(
            result.isValid(),
            "The password contains more than 1 lowercase char, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password with more than 1 lowercase character"
        )
    }

    @Test
    fun `ensure a password without any lowercase char is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneLowercaseCharacterValidationHandlerFixture.passwordDefinitionWithoutAnyLowercaseCharacter())
        assertFalse(
            result.isValid(),
            "The password does not contain any lowercase chars, but the validation handler considered the password as valid"
        )
        assertTrue(
            result.errors().isNotEmpty(),
            "The error list is empty for a invalid password without any lowercase chars"
        )
    }
}
