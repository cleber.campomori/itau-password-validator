package br.com.itau.password.validator.services.core.validation.handlers.impl

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.AtLeastOneUppercaseCharacterValidationHandlerFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneUppercaseCharacterValidationHandler
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AtLeastOneUppercaseCharacterValidationHandlerTests {

    private val _passwordValidationHandler: PasswordValidationHandler = AtLeastOneUppercaseCharacterValidationHandler()

    @Test
    fun `ensure a password with 1 uppercase char is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneUppercaseCharacterValidationHandlerFixture.passwordDefinitionWithOneUppercaseCharacter())
        assertTrue(
            result.isValid(),
            "The password contains 1 uppercase char, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password with 1 uppercase character"
        )
    }

    @Test
    fun `ensure a password with more than 1 uppercase char is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneUppercaseCharacterValidationHandlerFixture.passwordDefinitionWithMoreThanOneUppercaseCharacter())
        assertTrue(
            result.isValid(),
            "The password contains more than 1 uppercase char, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password with more than 1 uppercase character"
        )
    }

    @Test
    fun `ensure a password without any uppercase char is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneUppercaseCharacterValidationHandlerFixture.passwordDefinitionWithoutAnyUppercaseCharacter())
        assertFalse(
            result.isValid(),
            "The password does not contain any uppercase chars, but the validation handler considered the password as valid"
        )
        assertTrue(
            result.errors().isNotEmpty(),
            "The error list is empty for a invalid password without any uppercase chars"
        )
    }
}
