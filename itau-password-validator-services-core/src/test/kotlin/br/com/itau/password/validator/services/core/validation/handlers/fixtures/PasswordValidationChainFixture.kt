package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastNineCharactersValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneDigitCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneLowercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneSpecialCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneUppercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.NotContainRepeatedCharactersValidationHandler

object PasswordValidationChainFixture {

    fun fullPasswordValidationHandlerChain(): List<PasswordValidationHandler> =
        listOf(
            AtLeastNineCharactersValidationHandler(),
            AtLeastOneDigitCharacterValidationHandler(),
            AtLeastOneLowercaseCharacterValidationHandler(),
            AtLeastOneSpecialCharacterValidationHandler(),
            AtLeastOneUppercaseCharacterValidationHandler(),
            NotContainRepeatedCharactersValidationHandler()
        )

    fun passwordViolatingJustOneValidationHandler() = PasswordDefinition(
        password = "AbTp9!foo"
    )

    fun passwordViolatingMoreThanOneValidationHandler() = PasswordDefinition(
        password = "abtp9!foo"
    )

    fun passwordNotViolatingValidationHandlerChain() = PasswordDefinition(
        password = "AbTp9!fok"
    )
}
