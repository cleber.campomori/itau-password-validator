package br.com.itau.password.validator.services.core.validation.handlers.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object AtLeastOneUppercaseCharacterValidationHandlerFixture {

    fun passwordDefinitionWithOneUppercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "aBc123def@"
    )

    fun passwordDefinitionWithMoreThanOneUppercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "abC123DEF@!"
    )

    fun passwordDefinitionWithoutAnyUppercaseCharacter(): PasswordDefinition = PasswordDefinition(
        password = "abcdef123@"
    )
}
