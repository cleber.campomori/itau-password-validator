package br.com.itau.password.validator.services.core.validation.handlers.impl

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.NotContainRepeatedCharactersValidationHandlerFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.NotContainRepeatedCharactersValidationHandler
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class NotContainRepeatedCharactersValidationHandlerTests {

    private val _passwordValidationHandler: PasswordValidationHandler = NotContainRepeatedCharactersValidationHandler()

    @Test
    fun `ensure a password with 1 repeated char is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(NotContainRepeatedCharactersValidationHandlerFixture.passwordDefinitionWithOneRepeatedCharacter())
        assertFalse(
            result.isValid(),
            "The password contains 1 repeated char, but the validation handler considered the password as valid"
        )
        assertFalse(
            result.errors().isEmpty(),
            "The error list is empty for a valid password with 1 repeated character"
        )
    }

    @Test
    fun `ensure a password with more than 1 repeated char is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(NotContainRepeatedCharactersValidationHandlerFixture.passwordDefinitionWithMoreThanOneRepeatedCharacter())
        assertFalse(
            result.isValid(),
            "The password contains more than 1 repeated char, but the validation handler considered the password as valid"
        )
        assertFalse(
            result.errors().isEmpty(),
            "The error list is empty for a valid password with more than 1 repeated character"
        )
    }

    @Test
    fun `ensure a password without any repeated char is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(NotContainRepeatedCharactersValidationHandlerFixture.passwordDefinitionWithoutAnyRepeatedCharacter())
        assertTrue(
            result.isValid(),
            "The password does not contain any repeated chars, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password without any repeated chars"
        )
    }
}
