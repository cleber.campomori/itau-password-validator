package br.com.itau.password.validator.services.core.validation.handlers.impl

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.AtLeastNineCharactersValidationHandlerFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastNineCharactersValidationHandler
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AtLeastNineCharactersValidationHandlerTests {

    private val _passwordValidationHandler: PasswordValidationHandler = AtLeastNineCharactersValidationHandler()

    @Test
    fun `ensure a password with 9 characters is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastNineCharactersValidationHandlerFixture.passwordDefinitionWith9Characters())
        assertTrue(
            result.isValid(),
            "The password contains 9 characters, but the validation handler considered the password as invalid"
        )
        assertTrue(result.errors().isEmpty(), "The error list is not empty for a valid password with 9 characters")
    }

    @Test
    fun `ensure a password with more than 9 characters is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastNineCharactersValidationHandlerFixture.passwordDefinitionWithMoreThan9Characters())
        assertTrue(
            result.isValid(),
            "The password contains more than 9 characters, but the validation handler considered the password as invalid"
        )
        assertTrue(
            result.errors().isEmpty(),
            "The error list is not empty for a valid password with more than 9 characters"
        )
    }

    @Test
    fun `ensure a password with less than 9 characters is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastNineCharactersValidationHandlerFixture.passwordDefinitionWithLessThan9Characters())
        assertFalse(
            result.isValid(),
            "The password contains less than 9 characters, but the validation handler considered the password as valid"
        )
        assertTrue(
            result.errors().isNotEmpty(),
            "The error list is empty for a invalid password with less than 9 characters"
        )
    }
}
