package br.com.itau.password.validator.services.core.validation.handlers.impl

import br.com.itau.password.validator.services.core.validation.handlers.fixtures.AtLeastOneDigitCharacterValidationHandlerFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.base.PasswordValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneDigitCharacterValidationHandler
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class AtLeastOneDigitCharacterValidationHandlerTests {

    private val _passwordValidationHandler: PasswordValidationHandler = AtLeastOneDigitCharacterValidationHandler()

    @Test
    fun `ensure a password with 1 digit is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneDigitCharacterValidationHandlerFixture.passwordDefinitionWithOneDigitCharacter())
        assertTrue(
            result.isValid(),
            "The password contains 1 digit, but the validation handler considered the password as invalid"
        )
        assertTrue(result.errors().isEmpty(), "The error list is not empty for a valid password with 1 digit")
    }

    @Test
    fun `ensure a password with more than 1 digit is considered as valid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneDigitCharacterValidationHandlerFixture.passwordDefinitionWithMoreThanOneDigitCharacter())
        assertTrue(
            result.isValid(),
            "The password contains more than 1 digit, but the validation handler considered the password as invalid"
        )
        assertTrue(result.errors().isEmpty(), "The error list is not empty for a valid password with more than 1 digit")
    }

    @Test
    fun `ensure a password without any digit is considered as invalid`() {
        val result: PasswordValidationNotification =
            _passwordValidationHandler.handle(AtLeastOneDigitCharacterValidationHandlerFixture.passwordDefinitionWithoutAnyDigitCharacter())
        assertFalse(
            result.isValid(),
            "The password does not contain any digits, but the validation handler considered the password as valid"
        )
        assertTrue(result.errors().isNotEmpty(), "The error list is empty for a invalid password without any digit")
    }
}
