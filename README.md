# Itaú Password Validator [Backend Challenge]

This is an implementation of Itaú Backend Challenge. This solution consists of a RESTful Web API that validates a supplied password by some specific rules. [[Link to the challenge description](https://github.com/itidigital/backend-challenge)]

|**Author**:  |Cleber Lopes Campomori  |
|--|--|
|**E-mail**:  |cleber.campomori@gmail.com  |


## Involved programming languages, frameworks and tools

 - Kotlin [1.3.72];
 - Spring Boot [2.3.0.RELEASE];
 - Maven (wrapper included)
 - Ktlint (a Kotlin linter by [Pinterest](https://ktlint.github.io/)) [0.34.2];
 - jUnit [5.2.0]
 - Surefire (unit tests) and Failsafe (integration tests) Maven plugins.

## How to run the solution?

After you checkout the files of this repository, you can run the solution by performing default Spring Boot Maven goals.

The following commands are using the Maven wrapper: 

```sh
> cd [full path of project folder]
> ./mvnw clean install
> cd itau-password-validator-web
> ../mvnw spring-boot:run
```

If you have Maven installed on your environment, you can use the `mvn` instead of the wrapper:
```sh
> cd [full path of project folder]
> mvn clean install
> cd itau-password-validator-web
> mvn spring-boot:run
```

The default HTTP port for the application is `8080` (you can setup inside `application.yml` configuration file).

## Architectural aspects of the solution

This solution was developed by using an onion architecture style. The layers of the application were distributed through separate modules. Each module has a specific responsibility inside the application. The table below details about each module and your respective responsibility:

| Module | Responsibility |
|--|--|
| itau-password-validator-models | Contain anemic business models used by the application |
| itau-password-validator-services-core | Contain "pure" basic service structures to deal with password validation. On this layer, there is `PasswordService` interface, the validation strategies and validation chain implemented using a *Chain of Responsibility* combined with *Template Method* and *Strategy* |
| itau-password-validator-services-spring | Contain Spring Boot adapters (mainly IoC Spring annotations, like `@Service`) to use the core of Password Validation Services (`itau-password-validator-services-core`) together with a Spring application |
| itau-password-validator-web | Exposes a RESTful/web application by using Spring Boot to turn available HTTP Password Validation Services by using `services` and `model` layers |
| itau-password-validator-dtos | Contain just request and response representations for the HTTP operations. These representations were built by DTO classes |

Each module was designed to be fully decoupled of the other modules of the solution. This means that is possible use, for example, use `itau-password-validator-services-core` inside a non-Spring application, like a Vert.x application, a WebFlux application, a Ktor application or even an simple JSE application. `itau-password-validator-dtos` can be used, for example, to build a "*canonical model*" to set requests and responses for the application across different plaforms (e.g. a Web Application, a Desktop application and an Android application - these different solutions can share the module `itau-password-validation-dtos`).

The password validation process was built to be flexible and with structures extremely focused on pieces of the validation task. To reach this target, the validation process was built by using a mix of *Chain of Responsibility*, *Template Method*  and *Strategy* patterns. There are 6 pre-built validation handlers to be used together with the validation chain. This is the basic representation of this structure:

```
                      ------------------------------------------------
                      |          PasswordValidationHandler           |
                      ------------------------------------------------
                      | + setNext(handler: PasswordValidationHandler)|                        
                      | + handle(pDef: PasswordDefinition)           |
                      | #* isValid(pDef: PasswordDefinition)         |
                      | # reset()                                    |
                      | - isEndOfChain()                             |                                            
                      ------------------------------------------------
                                                          <<inherits>>
                                                               |
-------------------------------------------------              |
| AtLeastNineCharactersValidationHandler        |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |              |
-------------------------------------------------              |
                                                               |
-------------------------------------------------              |
| AtLeastOneDigitCharacterValidationHandler     |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |              |
-------------------------------------------------              |
                                                               |
-------------------------------------------------              |
| AtLeastOneLowercaseCharacterValidationHandler |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |              |
-------------------------------------------------              |
                                                               |
-------------------------------------------------              |
| AtLeastOneSpecialCharacterValidationHandler   |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |              |
-------------------------------------------------              |
                                                               |
-------------------------------------------------              |
| AtLeastOneUppercaseCharacterValidationHandler |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |              |
-------------------------------------------------              |
                                                               |
-------------------------------------------------              |
| NotContainRepeatedCharactersValidationHandler |              |
------------------------------------------------- >------------|
| # isValid(pDef: PasswordDefinition)           |
-------------------------------------------------
```

To control the password flow, there is the `PasswordValidationChain` class on `itau-password-validator-services-core`. The class contains a fluent API that allows add `PasswordValidationHandler` implementations by using the method `addValidationHandler()`, building the validation chain. After the validation chain is built, we can call the method `apply()`. This method will apply all the validation chain on the supplied password definition, returning a `PasswordValidationNotification` instance (*notification pattern*)

```
|-------------------------------------------------------------------------------------|
|                            PasswordValidationChain                                  |
|-------------------------------------------------------------------------------------|
| + addValidationHandler(handler: PasswordValidationHandler): PasswordValidationChain |
| + fun forPasswordDefinition(pDef: PasswordDefinition): PasswordValidationChain      |
| + fun apply(): PasswordValidationNotification                                       |
|-------------------------------------------------------------------------------------|
```

The `PasswordValidationNotification` class defines a "classic" notification:

```
|--------------------------------|
| PasswordValidationNotification | 
|--------------------------------|
| # addError(message: String)    |
| + errors(): List<String>       |
| + isValid(): Boolean           |
| # reset()                      |
|--------------------------------|
```

Using this classes, we can validate a password using the following example code:

```kotlin
val passwordValidationChain: PasswordValidationChain = 
	PasswordValidationChain()
		.addValidationHandler(AtLeastNineCharactersValidationHandler())
		.addValidationHandler(AtLeastOneDigitCharacterValidationHandler())		
		.addValidationHandler(AtLeastOneLowercaseCharacterValidationHandler())				
		.addValidationHandler(AtLeastOneSpecialCharacterValidationHandler())						
		.addValidationHandler(AtLeastOneUppercaseCharacterValidationHandler())								
		.addValidationHandler(NotContainRepeatedCharactersValidationHandler())
val validationResult: PasswordValidationNotification = 
	passwordValidationChain
		.forPasswordDefinition(PasswordDefinition("the-password"))  
	    .apply()
println(validationResult)
```

## RESTful aspects of the solution
Trying to follow REST principles as much as possible, the API exposes just one endpoint: a `POST` to `{{SERVER_URL}}:{{SERVER_PORT}}\passwords`. This endpoint can reply with three possibilities:

- a 204 HTTP Status (No Content), indicating that the provided password is valid;
- a 400 HTTP Status (Bad Request), indicating that the password cannot be validated. On this case, the response body contains a list of error messages called `errors`:
- a 500 HTTP Status (Internal Server Error), indicating an unexpected application failure.

The RESTful API contains a Swagger document attached. If you want to access, you can go to the address `http://{{SERVER_URL}}:{{SERVER_PORT}}/swagger-ui.html`.

Some examples of request and responses:

```
POST /passwords HTTP/1.1
Host: localhost:8080
Content-T: application/json
Content-Type: application/json

{
	"password": "abc123A@!"
}

----

HTTP Response Status: 204 [No content]
```

```
POST /passwords HTTP/1.1
Host: localhost:8080
Content-T: application/json
Content-Type: application/json

{
	"password": "abca"
}

----

HTTP Response Status: 400 [Bad Request]

{

"errors": [

"The password contains less than 9 characters",

"The password must contain at least one uppercase character",

"The password must contain at least 1 digit",

"he password must contain at least 1 special character",

"The password must not contain repeated characters"

]

}
```

> I decided to use the `POST` method just because it is more" common ". If I wanted to follow the REST principles more strictly, I think I should use the `PUT` method due to idempotency.