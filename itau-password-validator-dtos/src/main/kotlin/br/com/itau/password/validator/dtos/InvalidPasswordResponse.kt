package br.com.itau.password.validator.dtos

data class InvalidPasswordResponse(
    val errors: List<String> = listOf()
)
