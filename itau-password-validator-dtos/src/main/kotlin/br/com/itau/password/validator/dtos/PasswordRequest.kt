package br.com.itau.password.validator.dtos

/**
 * Data class to represent password validation request
 * @param password The password to be validated
 */
data class PasswordRequest(
    val password: String
)
