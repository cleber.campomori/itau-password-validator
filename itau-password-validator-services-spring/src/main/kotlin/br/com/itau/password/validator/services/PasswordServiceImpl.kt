package br.com.itau.password.validator.services

import br.com.itau.password.validator.models.PasswordDefinition
import br.com.itau.password.validator.services.core.PasswordService
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import br.com.itau.password.validator.services.validation.handlers.chain.PasswordValidationChain
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

/**
 * Password validation service Spring implementation
 * @see PasswordService
 */
@Service
class PasswordServiceImpl(
    private val passwordValidationChain: PasswordValidationChain
) : PasswordService {

    private val logger: Logger by lazy {
        LoggerFactory.getLogger(this::class.java)
    }

    override fun validatePassword(passwordDef: PasswordDefinition): PasswordValidationNotification {
        logger.debug("Validating password definition $passwordDef")
        val validationResult: PasswordValidationNotification =
            passwordValidationChain.forPasswordDefinition(passwordDef).apply()
        logger.debug("OK! Password definition $passwordDef was validated. Result: $validationResult")
        return validationResult
    }
}
