package br.com.itau.password.validator.services

import br.com.itau.password.validator.services.core.PasswordService
import br.com.itau.password.validator.services.fixtures.PasswordServiceImplFixture
import br.com.itau.password.validator.services.fixtures.PasswordValidationChainFixture
import br.com.itau.password.validator.services.notifications.PasswordValidationNotification
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

class PasswordServiceImplTests {

    private val _passwordService: PasswordService by lazy {
        PasswordServiceImpl(PasswordValidationChainFixture.fullPasswordValidationChain())
    }

    @Test
    fun `ensure a password that violates just one handler is invalid`() {
        val result: PasswordValidationNotification =
            _passwordService.validatePassword(PasswordServiceImplFixture.passwordDefinitionViolatingJustOneHandler())
        assertFalse(
            result.isValid(),
            "The password validation was true for a invalid password [${PasswordServiceImplFixture.passwordDefinitionViolatingJustOneHandler().password}]"
        )
        assertTrue(
            result.errors().size == 1,
            "Tue supplied password violates just one handler, but there are more than 1 error message [${PasswordServiceImplFixture.passwordDefinitionViolatingJustOneHandler().password}]"
        )
    }

    @Test
    fun `ensure a password that violates more than one handler is invalid`() {
        val result: PasswordValidationNotification =
            _passwordService.validatePassword(PasswordServiceImplFixture.passwordDefinitionViolatingMoreThanOneHandler())
        assertFalse(
            result.isValid(),
            "The password validation was true for a invalid password [${PasswordServiceImplFixture.passwordDefinitionViolatingMoreThanOneHandler().password}]"
        )
        assertTrue(
            result.errors().size > 1,
            "Tue supplied password violates more than one handler, but there are 1 or less error messages [${PasswordServiceImplFixture.passwordDefinitionViolatingJustOneHandler().password}]"
        )
    }

    @Test
    fun `ensure a password that does not violate handlers is valid`() {
        val result: PasswordValidationNotification =
            _passwordService.validatePassword(PasswordServiceImplFixture.passwordDefinitionWithValidPassword())
        assertTrue(
            result.isValid(),
            "The password validation was false for a valid password [${PasswordServiceImplFixture.passwordDefinitionWithValidPassword().password}]"
        )
        assertTrue(
            result.errors().isEmpty(),
            "Tue supplied password does not violate any handlers, but there are 1 or more error messages [${PasswordServiceImplFixture.passwordDefinitionViolatingJustOneHandler().password}]"
        )
    }
}
