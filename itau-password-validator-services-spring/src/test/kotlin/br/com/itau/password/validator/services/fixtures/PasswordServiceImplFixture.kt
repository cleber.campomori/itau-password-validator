package br.com.itau.password.validator.services.fixtures

import br.com.itau.password.validator.models.PasswordDefinition

object PasswordServiceImplFixture {

    fun passwordDefinitionViolatingJustOneHandler() = PasswordDefinition(
        password = "AbTp9!foo"
    )

    fun passwordDefinitionViolatingMoreThanOneHandler() = PasswordDefinition(
        password = "AbTp9foo"
    )

    fun passwordDefinitionWithValidPassword() = PasswordDefinition(
        password = "AbTp9!fok"
    )
}
