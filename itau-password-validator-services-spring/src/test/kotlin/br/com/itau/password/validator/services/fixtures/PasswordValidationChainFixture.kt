package br.com.itau.password.validator.services.fixtures

import br.com.itau.password.validator.services.validation.handlers.chain.PasswordValidationChain
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastNineCharactersValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneDigitCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneLowercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneSpecialCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.AtLeastOneUppercaseCharacterValidationHandler
import br.com.itau.password.validator.services.validation.handlers.impl.NotContainRepeatedCharactersValidationHandler

object PasswordValidationChainFixture {

    fun fullPasswordValidationChain(): PasswordValidationChain =
        PasswordValidationChain()
            .addValidationHandler(AtLeastNineCharactersValidationHandler())
            .addValidationHandler(AtLeastOneLowercaseCharacterValidationHandler())
            .addValidationHandler(AtLeastOneUppercaseCharacterValidationHandler())
            .addValidationHandler(AtLeastOneDigitCharacterValidationHandler())
            .addValidationHandler(AtLeastOneSpecialCharacterValidationHandler())
            .addValidationHandler(NotContainRepeatedCharactersValidationHandler())
}
