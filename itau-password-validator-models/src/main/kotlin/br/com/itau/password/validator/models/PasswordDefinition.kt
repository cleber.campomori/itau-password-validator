package br.com.itau.password.validator.models

/**
 * Data class to define a password model
 * @property password the required password
 */
data class PasswordDefinition(
    val password: String
)
